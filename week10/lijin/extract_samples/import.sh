#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

dataset="/0/DATA/0/matrix"
hd5file="zspc_n1328098x22268.gctx"
count="1,22268"
ddlfile="CPC005_A375_6H_X1_B3_DUO52HI53LO_K06.info"
outfile="CPC005_A375_6H_X1_B3_DUO52HI53LO_K06.bin"
new_h5file="CPC005_A375_6H_X1_B3_DUO52HI53LO_K06.h5"

set -v
h5dump -p -d "$dataset" --count="$count" --ddl="$ddlfile" -o "$outfile" -b "$hd5file"
sed -i "s/1328098/1/" "$ddlfile"
h5import "$outfile" -c "$ddlfile" -o "$new_h5file"
