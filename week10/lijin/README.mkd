# The directory contain two examples.

1. Extracting samples from LINCS level 4 data.

The scripts under `extract_samples` depends on hdf5 command line tools, with the
link as.

[https://www.hdfgroup.org/HDF5/doc/RM/Tools.html](https://www.hdfgroup.org/HDF5/doc/RM/Tools.html)

2. Reading zscores into R variables.

The example under `read_hdf5` show one command in library `rhdf5` to read
zscores. The stored data is in h5 format, containing zscores for one drug.

3. The extracted drug data is placed into `data` subdirectory. The probesets and sample names from level 4 data are also placed there.
