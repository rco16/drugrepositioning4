suppressPackageStartupMessages(library(samr))
n=100
m1=5
m2=5
res=SAM(
  cbind(matrix(runif(n*m1), nr=n), matrix(runif(n*m2), nr=n))
  , c(rep(1, m1), rep(2, m2))
  , resp.type='Two class unpaired'
  )
#str(res)
res$siggenes.table
