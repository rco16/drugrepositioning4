#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

set -v
namedir=AML001_CD34_24H_X1_F1B10
indir="s3://data.lincscloud.org/l1000/level1/${namedir}"
outdir=$(mktemp -d)

s3cmd sync "$indir" "$outdir"

echo $outdir
