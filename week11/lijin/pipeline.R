suppressPackageStartupMessages(library(affy))
suppressPackageStartupMessages(library(limma))
suppressPackageStartupMessages(library(hgu95av2.db))
suppressPackageStartupMessages(library(hgu133plus2.db))
suppressPackageStartupMessages(library(annotate))

suppressPackageStartupMessages(library(xlsx))
landmarkGenes <- read.xlsx("Landmark_Genes_n978.xlsx", sheetIndex=1)[,'Gene.Symbol']

datadir='/mnt/glusterfs_distributed/ragse-f2015/disease_dataset/GSE49566' # disease
metadata <- read.table("metadata.txt", header=T)
sampleNames <- metadata[,"Sample"]
filenames <- metadata[,"Filename"]
ab=ReadAffy(filenames=filenames, celfile.path=datadir, sampleNames=sampleNames)
eset=rma(ab, verbose=F)
DiseaseID=featureNames(eset)
DiseaseSymbol=getSYMBOL(DiseaseID, 'hgu95av2.db')
GenesDisease <- intersect(DiseaseSymbol, landmarkGenes) # disease genes
str(GenesDisease)

suppressPackageStartupMessages(library(rhdf5)) # drug
lincslevel4 <- '/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx'
DrugID = gsub(' ', '', h5read(lincslevel4, name='0/META/ROW/id'))
DrugSymbol=getSYMBOL(as.vector(DrugID), 'hgu133plus2.db')
GenesDrug <- intersect(DrugSymbol, landmarkGenes) # drug genes
str(GenesDrug)

commonGenes <- intersect(GenesDisease, GenesDrug)
str(commonGenes)

IndexDisease <- match(commonGenes, DiseaseSymbol) # disease query
suppressPackageStartupMessages(library(samr))
sampleType <- metadata[,'Type']
exprsDisease <- exprs(eset)[IndexDisease,]
rownames(exprsDisease) <- commonGenes
resDisease<-SAM(x=exprsDisease, y=sampleType, resp.type='Two class unpaired', genenames=commonGenes, geneid=commonGenes)
queryDisease <- rep(0, length(commonGenes))
names(queryDisease) <- commonGenes
if (resDisease$siggenes.table$ngenes.up > 0) {
  queryDisease[resDisease$siggenes.table$genes.up[,'Gene Name']] <- 1
}
if (resDisease$siggenes.table$ngenes.lo > 0) {
  queryDisease[resDisease$siggenes.table$genes.lo[,'Gene Name']] <- -1
}

IndexDrug <- match(commonGenes, DrugSymbol) # drug zscore
drugZscore <- h5read(lincslevel4, index=list(NULL,1), name = "/0/DATA/0/matrix")[,1]
experimentDrug <- drugZscore[IndexDrug]
names(experimentDrug) <- commonGenes

source("my_connectivity_score.R")
my_connectivity_score(experimentDrug, queryDisease)

