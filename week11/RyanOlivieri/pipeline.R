suppressPackageStartupMessages(library(samr))
suppressPackageStartupMessages(library(affydata))
suppressPackageStartupMessages(library(affy))
suppressPackageStartupMessages(library(hgu133plus2.db))
suppressPackageStartupMessages(library(rhdf5))
source("probesTOgenes.R")
source("my_connectivity_score.R")

## read affy

##celfile.path=system.file("disease_GSE49566/disease_data", package="affydata")  

meta_data = read.table("metadata.txt",header=T)

disease_data = invisible(ReadAffy(filenames=as.vector(as.matrix(meta_data[3])), celfile.path="/mnt/glusterfs_distributed/ragse-f2015/disease_dataset/GSE49566") )
## rma

expression_matrix = invisible(exprs(affy::rma(disease_data)))
#str(expression_matrix)   

## probeset to gene

gene_names = invisible(probesTOgenes(rownames(expression_matrix)))

## samr
## larger than number of genes in human body
## will be intersected with the genes from the drug sample
n=25000
res_sam =invisible(SAM(
    expression_matrix
  , as.factor(as.vector(as.matrix(meta_data[2])))
  , resp.type='Two class unpaired'
  , genenames = gene_names
  ))

#print("RES_SAM")
#print(res_sam)

#Convert the up and down regulatd genes into one induced vector from the samr results

if(!is.null(res_sam$siggenes.table$genes.lo) && !is.null(nrow(res_sam$siggenes.table$genes.lo)))
{
    genesLo = res_sam$siggenes.table$genes.lo[,2]
} else {
    genesLo = res_sam$siggenes.table$genes.lo[2]
}

indexLo = sapply(genesLo, FUN=function(x) as.integer(2))

if(!is.null(res_sam$siggenes.table$genes.up) && !is.null(nrow(res_sam$siggenes.table$genes.up))) {
    genesUp = res_sam$siggenes.table$genes.up[,2]
} else {
    genesUp = res_sam$siggenes.table$genes.up[2]
}

indexUp = sapply(genesUp, FUN=function(x) as.integer(x))
query = c(rep(0,n))

if(length(indexLo) > 0){
    query[as.vector(indexLo)] = -1
}

if(length(indexUp) > 0){
    query[as.vector(indexUp)] = 1
}

geneNames = paste(rep("g",n),as.character(seq(1:n)),sep='')
names(query) = geneNames

print(query)


## intersect query with L1000 genes
drug_genes = as.vector(as.matrix(read.table("L1000Genes.txt")))
query_intersect = query[drug_genes]

## drug data

file = '/mnt/glusterfs_distributed/ragse-f2015/lincs_l4_data/zspc_n1328098x22268.gctx'
Probes = h5read(file, name='0/META/ROW')
n_Probes = length(Probes$id)
Drugs = h5read(file, name='0/META/COL')
n_Drugs = length(Drugs$id)
firstDrug = h5read(file, index=list(NULL,1), name = "/0/DATA/0/matrix")
str(firstDrug)


## compare

score = my_connectivity_score(firstDrug,query_intersect)

print(score)
