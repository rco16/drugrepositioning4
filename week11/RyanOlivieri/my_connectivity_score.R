my_connectivity_score = function (experiment, query)
{
    
    sets.up <- which(query == 1)
    sets.down <- which(query == -1)

    ## reverse matrix
    experimentR =  length(experiment) - rank(experiment) + 1

    raw.score <- score(experimentR[sets.up], experimentR[sets.down], length(experimentR))
 }

.ks <- function( V, n ) {
  t <- length( V )

  if( t == 0 )  {
    return( 0 )
  } else {

    if ( is.unsorted( V ) )
      V <- sort( V )
    d <- (1:t) / t - V / n
    a <- max( d )
    b <- -min( d ) + 1 / t
    ifelse( a > b, a, -b )
  }
}

score <- function( V_up, V_down, n ) {
  ks_up <- .ks( V_up, n )
  ks_down <- .ks( V_down, n )
  ifelse( sign( ks_up ) == sign( ks_down ), 0, ks_up - ks_down )
}
